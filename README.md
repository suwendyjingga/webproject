1. Student Name and ID
Ivan Yufen Stefanus / 5980620
Suwendy Jingga / 5980619

2. Brief Summary of the project
This project called YourWeather.com. Basically, this is a website that provides you with world clock time and weather(including humidity, wind, rain and pressure) of several cities around the world. Especially Asia. We got the weather data from API of openweather.org and clock from timeanddate.com. 

3. List of framework and library used
https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.min.js
https://use.fontawesome.com/b88dde9015.js

4. List of API used
OpenWeather.org
b3c39a1a65c0a1d7c970d05f3ee3deef

5. List of known bugs
In our opinion, we couldn't find any bugs.

That's all. Thank you, Sir.

